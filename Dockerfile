FROM archlinux:latest

# INSTALL BASE PACKAGES
RUN pacman -Syu --noconfirm
RUN pacman -S --noconfirm sudo base-devel fakeroot git \
boost root python python2

# CLEANUP
RUN sudo pacman -Scc --noconfirm && sudo pacman -Rns --noconfirm $(pacman -Qtdq) || echo 'Nothing to remove'

# SETUP WORKDIR
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
